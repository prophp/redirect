<?php

class CsvRedirect
{
    private $redirects;

    private $ipWhitelist;

    private $requestIp;

    private $fallbackUrl;

    private $adminToken;

    public function __construct(
        $redirectFilePath,
        $delimiter = ';',
        $fallbackUrlFilePath = null,
        $ipWhitelistFilePath = null,
        $adminTokenFilePath = null
    )
    {
        $this->setRedirects($redirectFilePath, $delimiter);
        $this->setWhitelistedIps($ipWhitelistFilePath, $delimiter);
        $this->setAdminToken($adminTokenFilePath);
        $this->setFallbackUrl($fallbackUrlFilePath);
    }

    private function setFallbackUrl($fallbackUrlFilePath)
    {
        if ($fallbackUrlFilePath === null || !file_exists($fallbackUrlFilePath)) {
            $this->fallbackUrl = null;
            return;
        }

        $this->fallbackUrl = trim(file_get_contents($fallbackUrlFilePath));
    }

    private function setAdminToken($adminTokenFilePath)
    {
        if ($adminTokenFilePath === null || !file_exists($adminTokenFilePath)) {
            $this->adminToken = null;
            return;
        }

        $this->adminToken = trim(file_get_contents($adminTokenFilePath));
    }

    private function setRedirects($redirectFilePath, $delimiter)
    {
        if(!file_exists($redirectFilePath)){
            $this->redirects = [];
            return;
        }

        $csvFile = file($redirectFilePath);
        $data = [];
        foreach ($csvFile as $line) {
            list($from, $to) = str_getcsv($line, $delimiter);
            $data[] = [$from, $to];
        }

        $this->redirects = $data;
    }

    private function setWhitelistedIps($ipWhitelistFilePath, $delimiter)
    {
        if ($ipWhitelistFilePath === null || !file_exists($ipWhitelistFilePath)) {
            $this->ipWhitelist = [];
            return;
        }

        $this->ipWhitelist = array_map(
            function ($ip) {
                return trim($ip);
            },
            explode($delimiter, file_get_contents($ipWhitelistFilePath))
        );
    }

    private function setRequestIp($SERVER)
    {
        if (!empty($SERVER['HTTP_CLIENT_IP'])) {
            $this->requestIp = $SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($SERVER['HTTP_X_FORWARDED_FOR'])) {
            $this->requestIp = $SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $this->requestIp = $SERVER['REMOTE_ADDR'];
        }
    }


    public function execute($SERVER)
    {
        if(empty($this->redirects)){
            return;
        }

        $this->setRequestIp($SERVER);

        if (
            in_array($this->requestIp, $this->ipWhitelist)
            ||
            ($this->adminToken !== null && strpos($SERVER['QUERY_STRING'], 'admin_token=' . $this->adminToken) !== false)
        ) {
            return;
        }

        $fullLink = (isset($SERVER['HTTPS']) && $SERVER['HTTPS'] === 'on' ? "https" : "http") . "://{$SERVER['HTTP_HOST']}{$SERVER['REQUEST_URI']}";

        $urlToRedirectTo = null;

        foreach ($this->redirects as $line) {
            if (
                in_array($fullLink, [
                    $line[0],
                    $line[0] . "/",
                    $line[0] . "//",
                ], true)
                ||
                in_array(explode("?", $fullLink)[0], [
                    $line[0],
                    $line[0] . "/",
                    $line[0] . "//",
                ], true)
            ) {
                $urlToRedirectTo = $line[1];
                break;
            }
        }

        if ($urlToRedirectTo !== null) {
            header("Location: $urlToRedirectTo", TRUE, 301);
            exit;
        }

        if ($this->fallbackUrl !== null) {
            header("Location: " . $this->fallbackUrl, TRUE, 301);
        }
    }
}



