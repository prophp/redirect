<?php

require_once __DIR__ . '/CsvRedirect.updated.php';

(new CsvRedirect(
    __DIR__ . '/data/redirect.full-list.csv',
    ';',
    __DIR__ . '/data/fallback-url.txt',
    __DIR__ . '/data/ip-whitelist.csv' ,
    __DIR__ . '/data/admin-token.txt')
)->execute($_SERVER);