<?php

class CsvRedirect
{
    private $data;

    private $logPath;

    public function __construct($configFilePath, $delimiter, $logPath)
    {
        $this->data = $this->parseFile($configFilePath, $delimiter);
        $this->logPath = $logPath;
    }

    private function parseFile($configFilePath, $delimiter)
    {
        $csvFile = file($configFilePath);
        $data = [];
        foreach ($csvFile as $line) {
            $data[] = str_getcsv($line, $delimiter);
        }

        return $data;
    }

    private function stripWildcard($fullWildcard)
    {
        return rtrim($fullWildcard, '*'); // . '/';
    }

    private function prepareRedirectUrlWithWildcards($fullLink)
    {
        $wildcard = null;

        foreach ($this->data as $line) {

            if (empty($line[3])) {
                continue;
            }

            $configWildcard = $this->stripWildcard($line[3]);

            if (strpos($fullLink, $configWildcard) === 0) {
                $wildcard = [$configWildcard, $line[4]]; //$this->stripWildcard($line[4])];
                break;
            }

        }

        if ($wildcard === null) {
            return null;
        }

	return $wildcard[1];
        //return str_replace($wildcard[0], $wildcard[1], $fullLink);
    }

    private function logRequest($fullLink, $urlToRedirectTo)
    {
        file_put_contents(
            $this->logPath,
            (new DateTime())->format("Y-m-d H:i:s") . " $fullLink -> $urlToRedirectTo\n",
            FILE_APPEND
        );
    }

    public function execute($SERVER)
    {
        $fullLink = (isset($SERVER['HTTPS']) && $SERVER['HTTPS'] === 'on' ? "https" : "http") . "://{$SERVER['HTTP_HOST']}{$SERVER['REQUEST_URI']}";

        $urlToRedirectTo = null;

        if (
		strpos($fullLink, 'https://ee.olympic-casino.com/admin') === 0 ||
		strpos($fullLink, 'https://sk.olympic-casino.com/admin') === 0 ||
		strpos($fullLink, 'https://lv.olympic-casino.com/admin') === 0 ||
		strpos($fullLink, 'https://lt.olympic-casino.com/admin') === 0 ||
		strpos($fullLink, 'https://casinomalta.com.mt/admin') === 0
	){
            $this->logRequest($fullLink, $urlToRedirectTo);
            return null;
        }

        foreach ($this->data as $line) {
            if ($line[0] === $fullLink) {
                $urlToRedirectTo = $line[1];
            }
        }

        if ($urlToRedirectTo === null) {
            $urlToRedirectTo = $this->prepareRedirectUrlWithWildcards($fullLink);
        }

        if ($urlToRedirectTo !== null) {
            $this->logRequest($fullLink, $urlToRedirectTo);
            header("Location: $urlToRedirectTo", TRUE, 301);
            exit;
        }

        $this->logRequest($fullLink, $urlToRedirectTo);
    }
}


